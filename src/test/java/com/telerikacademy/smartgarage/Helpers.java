package com.telerikacademy.smartgarage;

import com.telerikacademy.smartgarage.models.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static Users createDefaultUser() {
        int count = 0;

        return createMockUser("User", "mock" + ++count + "@mail.com");
    }

    public static Users createMockUser() {

        return createMockUser("User", "mockU@mail.com");
    }

    public static Users createMockAdmin() {
        return createMockUser("Employee", "mockA@mail.com");
    }

    public static Users createMockUser(String role, String email) {
        var mockUser = new Users();

        mockUser.setUserId(999);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setUsername(email.substring(0, 5));
        mockUser.setPassword("MockPassword");
        mockUser.setEmail(email);
        mockUser.setPhoneNumber("0123456789");
        mockUser.setUserRole(createMockRole(role));

        return mockUser;
    }

    public static UserRole createMockRole(String role) {
        var mockRole = new UserRole();

        mockRole.setUserRoleId(999);
        mockRole.setRoleName(role);

        return mockRole;
    }

    public static RegisteredVehicles createMockRegVehicle(Users owner, String modelName, String brandName) {
        var mockRV = new RegisteredVehicles();

        mockRV.setRegisteredVehicleId(999);
        mockRV.setLicensePlate("AB0123CD");
        mockRV.setVin("1234567891234567");
        mockRV.setOwner(owner);
        mockRV.setVehicle(createMockVehicle(modelName, brandName));

        return mockRV;
    }

    public static Vehicles createMockVehicle(String modelName, String brandName) {
        var mockVehicle = new Vehicles();

        mockVehicle.setVehicleId(999);
        mockVehicle.setModel(createMockModel(modelName, brandName));
        mockVehicle.setYear(2022);

        return mockVehicle;
    }

    public static Models createMockModel(String modelName, String brandName) {
        var mockModel = new Models();

        mockModel.setModelId(999);
        mockModel.setModelName(modelName);
        mockModel.setBrand(createMockBrand(brandName));

        return mockModel;
    }

    public static Brands createMockBrand(String brandName) {
        var mockBrand = new Brands();

        mockBrand.setBrandId(999);
        mockBrand.setBrandName(brandName);

        return mockBrand;
    }

    public static Visits createMockVisit() {
        var mockVisit = new Visits();

        mockVisit.setVisitId(999);
        mockVisit.setRegisteredVehicle(createMockRegVehicle(createMockUser(), "model", "brand"));
        mockVisit.setServicesList(createMockServicesSet());
        mockVisit.setStartDate(LocalDate.MIN);
        mockVisit.setEndDate(LocalDate.MAX);
        mockVisit.setStatus(createMockVisitStatus());
        mockVisit.setTotalPrice(600);

        return mockVisit;
    }

    public static Services createMockService(String name, double price) {
        var mockService = new Services();

        mockService.setServiceId(999);
        mockService.setServiceName("MockService");
        mockService.setPrice(price);

        return mockService;
    }

    public static Set<Services> createMockServicesSet() {
        var mockServices = new HashSet<Services>();

        for (int i = 1; i <= 3; i++) {
            String name = "mockService" + i;
            double price = i * 100;

            mockServices.add(createMockService(name, price));
        }

        return mockServices;
    }

    public static VisitStatuses createMockVisitStatus() {
        var mockStatus = new VisitStatuses();

        mockStatus.setStatusId(999);
        mockStatus.setStatusName("MockStatus");

        return mockStatus;
    }
}
