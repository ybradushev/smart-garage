package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.VisitsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class VisitsServiceImplTests {

    @Mock
    VisitsRepository visitsRepository;

    @InjectMocks
    VisitsServiceImpl visitsService;

    @Test
    void getAll_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> visitsService.getAllVisits(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(), user));
    }

    @Test
    void getAll_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();

        Mockito.when(visitsRepository.filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        visitsService.getAllVisits(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(), admin);

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .filter(Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    void getById_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var visit = createMockVisit();
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> visitsService.getVisitById(visit.getVisitId(), user));
    }

    @Test
    void getById_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var visit = createMockVisit();

        Mockito.when(visitsRepository.getByField("visitId", visit.getVisitId()))
                .thenReturn(visit);

        // Act
        visitsService.getVisitById(visit.getVisitId(), admin);

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .getByField("visitId", visit.getVisitId());
    }

    @Test
    void createVisit_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var visit = createMockVisit();
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> visitsService.createVisit(visit, user));
    }

    @Test
    void createVisit_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var visit = createMockVisit();

        Mockito.doNothing().when(visitsRepository).create(visit);

        // Act
        visitsService.createVisit(visit, admin);

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .create(visit);
    }

    @Test
    void updateVisit_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var visit = createMockVisit();
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> visitsService.updateVisit(visit, user));
    }

    @Test
    void updateVisit_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var visit = createMockVisit();

        Mockito.doNothing().when(visitsRepository).update(visit);

        // Act
        visitsService.updateVisit(visit, admin);

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .update(visit);
    }

    @Test
    void deleteVisit_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var visit = createMockVisit();
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> visitsService.deleteVisit(visit.getVisitId(), user));
    }

    @Test
    void deleteVisit_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var visit = createMockVisit();

        Mockito.doNothing().when(visitsRepository).delete(visit.getVisitId());

        // Act
        visitsService.deleteVisit(visit.getVisitId(), admin);

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .delete(visit.getVisitId());
    }

    @Test
    void getByStatus_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var visit = createMockVisit();

        Mockito.when(visitsRepository.getByStatus(visit.getStatus().getStatusName()))
                .thenReturn(new ArrayList<>());

        // Act
        visitsService.getVisitsByStatus(visit.getStatus().getStatusName());

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .getByStatus(visit.getStatus().getStatusName());
    }

    @Test
    void getByUser_should_callRepository_whenCorrectDetails() {
        // Arrange
        var user = createMockUser();

        Mockito.when(visitsRepository.getByUser(user.getUserId(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        // Act
        visitsService.getByUser(user.getUserId(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty());

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .getByUser(user.getUserId(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty());
    }

    @Test
    void revertStatus_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var visit = createMockVisit();
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> visitsService.revertStatus(visit, user));
    }

    @Test
    void revertStatus_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var visit = createMockVisit();

        Mockito.doNothing().when(visitsRepository).update(visit);

        // Act
        visitsService.revertStatus(visit, admin);

        // Assert
        Mockito.verify(visitsRepository, Mockito.times(1))
                .update(visit);
    }
}
