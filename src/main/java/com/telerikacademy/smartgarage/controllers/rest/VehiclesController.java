package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.dto.*;
import com.telerikacademy.smartgarage.services.contracts.BrandsService;
import com.telerikacademy.smartgarage.services.contracts.ModelsService;
import com.telerikacademy.smartgarage.services.contracts.RegisteredVehiclesService;
import com.telerikacademy.smartgarage.services.contracts.VehiclesService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.mappers.VehicleMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/vehicles")
public class VehiclesController {
    private final BrandsService brandsService;
    private final ModelsService modelsService;
    private final VehiclesService vehiclesService;
    private final RegisteredVehiclesService registeredVehiclesService;
    private final AuthenticationHelper authenticationHelper;
    private final VehicleMapper vehicleMapper;

    public VehiclesController(BrandsService brandsService,
                              ModelsService modelsService,
                              VehiclesService vehiclesService,
                              RegisteredVehiclesService registeredVehiclesService,
                              AuthenticationHelper authenticationHelper,
                              VehicleMapper vehicleMapper) {

        this.brandsService = brandsService;
        this.modelsService = modelsService;
        this.vehiclesService = vehiclesService;
        this.registeredVehiclesService = registeredVehiclesService;
        this.authenticationHelper = authenticationHelper;
        this.vehicleMapper = vehicleMapper;
    }

    @GetMapping
    public List<Vehicles> getAllVehicles(@RequestHeader HttpHeaders headers) {

        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            return vehiclesService.getAllVehicles();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{vehicleId}")
    public Vehicles getVehicleById(@PathVariable int vehicleId) {
        try {
            return vehiclesService.getVehicleById(vehicleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void createVehicle(@Valid VehicleDTO vehicleDTO, @RequestHeader HttpHeaders headers) {
        try {
            Vehicles vehicle = vehicleMapper.createVehicleDTOtoObject(vehicleDTO);
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            vehiclesService.createVehicle(vehicle, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{vehicleId}")
    public void updateVehicle(@PathVariable int vehicleId, @Valid VehicleDTO vehicleDTO,
                              @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            Vehicles vehicle = vehicleMapper.updateVehicleDTOtoObject(vehicleDTO, vehicleId);

            vehiclesService.updateVehicle(vehicle, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{vehicleId}")
    public void deleteVehicle(@PathVariable int vehicleId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            vehiclesService.deleteVehicle(vehicleId, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/brands")
    public List<Brands> getAllBrands(@RequestHeader HttpHeaders headers) {

        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            return brandsService.getAllBrands();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/brands/{brandId}")
    public Brands getBrandById(@PathVariable int brandId) {
        try {
            return brandsService.getBrandById(brandId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/brands")
    public void createBrand(@Valid BrandDTO brandDTO, @RequestHeader HttpHeaders headers) {
        try {
            Brands brand = vehicleMapper.createBrandDTOtoObject(brandDTO);
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            brandsService.createBrand(brand, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/brands/{brandId}")
    public void updateBrand(@PathVariable int brandId, @Valid BrandDTO brandDTO,
                            @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            Brands brand = vehicleMapper.updateBrandDTOtoObject(brandDTO, brandId);

            brandsService.updateBrand(brand, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/brands/{brandId}")
    public void deleteBrand(@PathVariable int brandId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            brandsService.deleteBrand(brandId, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/models")
    public List<Models> getAllModels(@RequestHeader HttpHeaders headers) {

        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            return modelsService.getAllModels();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/models/{modelId}")
    public Models getModelById(@PathVariable int modelId) {
        try {
            return modelsService.getModelById(modelId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/brands/{brandId}/models")
    public List<Models> getModelByBrandId(@PathVariable int brandId) {
        try {
            return modelsService.getModelsByBrandId(brandId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/models")
    public void createModel(@Valid ModelDTO modelDTO, @RequestHeader HttpHeaders headers) {
        try {
            Models model = vehicleMapper.createModelDTOtoObject(modelDTO);
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            modelsService.createModel(model, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/models/{modelId}")
    public void updateModel(@PathVariable int modelId, @Valid ModelDTO modelDTO,
                            @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            Models model = vehicleMapper.updateModelDTOtoObject(modelDTO, modelId);

            modelsService.updateModel(model, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/models/{modelId}")
    public void deleteModel(@PathVariable int modelId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            modelsService.deleteModel(modelId, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/registered")
    public List<RegisteredVehicles> getAllRegisteredVehicles(@RequestParam(required = false) Optional<String> value,
                                                             @RequestParam(required = false) Optional<String> type,
                                                             @RequestHeader HttpHeaders headers) {

        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            return registeredVehiclesService.search(value, type, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/registered/{registeredVehicleId}")
    public RegisteredVehicles getRegisteredVehicleById(@PathVariable int registeredVehicleId) {
        try {
            return registeredVehiclesService.getRegisteredVehicleById(registeredVehicleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/registered/users/{userId}")
    public List<RegisteredVehicles> getRegisteredVehiclesByUser(@PathVariable int userId) {
        try {
            return registeredVehiclesService.getVehiclesByOwner(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/registered")
    public void createRegisteredVehicle(@Valid CreateRegVehicleDTO regVehicleDTO, @RequestHeader HttpHeaders headers) {
        try {
            RegisteredVehicles vehicle = vehicleMapper.createRegVehicleDTOtoObject(regVehicleDTO);
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            registeredVehiclesService.createRegisteredVehicle(vehicle, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/registered{registeredVehicleId}")
    public void updateRegisteredVehicle(@PathVariable int registeredVehicleId, @Valid UpdateRegVehicleDTO regVehicleDTO,
                                        @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            RegisteredVehicles vehicle = vehicleMapper.updateRegVehicleDTOtoObject(regVehicleDTO, registeredVehicleId);
            registeredVehiclesService.updateRegisteredVehicle(vehicle, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/registered/{registeredVehicleId}")
    public void deleteRegisteredVehicle(@PathVariable int registeredVehicleId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            registeredVehiclesService.deleteVehicle(registeredVehicleId, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/registered/filter")
    public List<RegisteredVehicles> filter(@RequestParam(required = false) Optional<Integer> year,
                                           @RequestParam(required = false) Optional<Integer> brandId,
                                           @RequestParam(required = false) Optional<Integer> modelId,
                                           @RequestParam(required = false) Optional<String> sort,
                                           @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            return registeredVehiclesService.filter(year, brandId, modelId, sort, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


}