package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.models.dto.VisitDTO;
import com.telerikacademy.smartgarage.services.contracts.VisitsService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.mappers.VisitsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/api/visits")
public class VisitsController {
    private final VisitsService visitsService;
    private final VisitsMapper visitsMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public VisitsController(VisitsService visitsService, VisitsMapper visitsMapper, AuthenticationHelper authenticationHelper) {
        this.visitsService = visitsService;
        this.visitsMapper = visitsMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Visits> getAllVisits(@RequestParam(required = false) Optional<String> startDate,
                                     @RequestParam(required = false) Optional<String> endDate,
                                     @RequestParam(required = false) Optional<String> keyword,
                                     @RequestParam(required = false) Optional<String> type,
                                     @RequestParam(required = false) Optional<String> sort,
                                     @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            return visitsService.getAllVisits(keyword, type, startDate, endDate, sort, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{visitId}")
    public Visits getVisitById(@PathVariable int visitId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            return visitsService.getVisitById(visitId, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public void createVisit(@RequestBody VisitDTO visitDTO, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            Visits visitToCreate = visitsMapper.createDtoToObject(visitDTO);

            visitsService.createVisit(visitToCreate, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{visitId}")
    public void deleteVisit(@PathVariable int visitId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            visitsService.deleteVisit(visitId, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
