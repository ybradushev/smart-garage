package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.services.contracts.VisitsService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/employee/home")
public class EmployeeHomeMvcController {
    private final VisitsService visitsService;
    private final AuthenticationHelper authenticationHelper;

    @ModelAttribute("inProgressTasks")
    public List<Visits> getInProgressTasks() {
        return visitsService.getVisitsByStatus("In progress");
    }

    @ModelAttribute("notStartedTasks")
    public List<Visits> getNotStartedTasks() {
        return visitsService.getVisitsByStatus("Not started");
    }

    @ModelAttribute("doneTasks")
    public List<Visits> getDoneTasks() {
        return visitsService.getVisitsByStatus("Done");
    }

    @Autowired
    public EmployeeHomeMvcController(VisitsService visitsService, AuthenticationHelper authenticationHelper) {
        this.visitsService = visitsService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showEmployeeHomePage(Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            authenticationHelper.tryGetUser(session);
            return "employee-home";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }
}
