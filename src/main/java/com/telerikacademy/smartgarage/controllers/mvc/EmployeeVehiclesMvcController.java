package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.models.RegisteredVehicles;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.services.contracts.RegisteredVehiclesService;
import com.telerikacademy.smartgarage.services.contracts.VehiclesService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/vehicles")
public class EmployeeVehiclesMvcController {
    private final VehiclesService vehiclesService;
    private final RegisteredVehiclesService registeredVehiclesService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeVehiclesMvcController(VehiclesService vehiclesService, RegisteredVehiclesService registeredVehiclesService, AuthenticationHelper authenticationHelper) {
        this.vehiclesService = vehiclesService;
        this.registeredVehiclesService = registeredVehiclesService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("vehicles")
    public List<RegisteredVehicles> populateVehicles() {
        return registeredVehiclesService.getAllVehicles();
    }

    @GetMapping
    public String showVehiclesPage(Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            authenticationHelper.tryGetUser(session);
            return "vehicles";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/search")
    public String searchServices(@RequestParam(required = false) Optional<String> keyword,
                                 @RequestParam(required = false) Optional<String> type,
                                 Model model, HttpSession session) {
        Users user;
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            user = authenticationHelper.tryGetUser(session);
            List<RegisteredVehicles> rv = registeredVehiclesService.search(keyword, type, user);
            model.addAttribute("vehicles", rv);
            return "vehicles";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }
}
