package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicateEmailException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicatePhoneNumberException;
import com.telerikacademy.smartgarage.models.PasswordTokens;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.PasswordTokensRepository;
import com.telerikacademy.smartgarage.repositories.contracts.RegisteredVehicleRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UsersRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitsRepository;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import com.telerikacademy.smartgarage.utils.EmailHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.telerikacademy.smartgarage.utils.helpers.GrantsHelper.isAdmin;

@Service
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;
    private final VisitsRepository visitsRepository;
    private final RegisteredVehicleRepository vehicleRepository;
    private final PasswordTokensRepository tokensRepository;
    private final EmailHelper emailHelper;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository, VisitsRepository visitsRepository, RegisteredVehicleRepository vehicleRepository, PasswordTokensRepository tokensRepository,
                            EmailHelper emailHelper) {
        this.usersRepository = usersRepository;
        this.visitsRepository = visitsRepository;
        this.vehicleRepository = vehicleRepository;
        this.tokensRepository = tokensRepository;
        this.emailHelper = emailHelper;
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.getAll();
    }

    @Override
    public Users getUserById(int id) {
        return usersRepository.getByField("userId", id);
    }

    @Override
    public Users getByEmail(String email) {
        return usersRepository.getByField("email", email);
    }

    @Override
    public Users getUserByUsername(String username) {
        return usersRepository.getByField("username", username);
    }

    @Override
    public List<Users> getAllCustomers() {
        return usersRepository.getAllCustomers();
    }

    @Override
    public void changePasswordWithoutToken(Users operatingUser, String password) {
        if (operatingUser.getPassword().equals(password)) {
            throw new DuplicateEntityException("You have to enter a different password from the old one");
        }

        operatingUser.setPassword(password);
        usersRepository.update(operatingUser);
    }

    @Override
    public void createUser(Users user, Users operatingUser) {
        boolean emailExists = true;
        boolean phoneNumberExists = true;

        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create user accounts");
        }

        try {
            usersRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }

        try {
            usersRepository.getByField("phoneNumber", user.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            phoneNumberExists = false;
        }

        if (emailExists) {
            throw new DuplicateEmailException(user.getEmail());
        }

        if (phoneNumberExists) {
            throw new DuplicatePhoneNumberException(user.getPhoneNumber());
        }

        usersRepository.create(user);
        emailHelper.constructLoginCredentialsEmail(user);
    }

    @Override
    public void updateUser(Users userToUpdate, Users operatingUser) {
        boolean emailExists = true;
        boolean phoneNumberExists = true;

        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can update users");
        }

        try {
            usersRepository.getByField("email", userToUpdate.getEmail());
        } catch (EntityNotFoundException e) {
            emailExists = false;
        }

        try {
            usersRepository.getByField("phoneNumber", userToUpdate.getPhoneNumber());
        } catch (EntityNotFoundException e) {
            phoneNumberExists = false;
        }

        if (emailExists) {
            throw new DuplicateEmailException(userToUpdate.getEmail());
        }

        if (phoneNumberExists) {
            throw new DuplicatePhoneNumberException(userToUpdate.getPhoneNumber());
        }

        usersRepository.update(userToUpdate);
    }

    @Override
    public void deleteUser(int userId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete users");
        }
        visitsRepository.makeVisitsWithDefaultVehicle(userId);
        vehicleRepository.deleteVehiclesByOwner(userId);
        usersRepository.delete(userId);
    }

    @Override
    public List<Users> search(Optional<String> keyword, Optional<String> type, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete users");
        }

        return usersRepository.search(keyword, type);
    }

    @Override
    public void createPasswordToken(Users operatingUser) {
        PasswordTokens token = new PasswordTokens(UUID.randomUUID().toString(), operatingUser);
        tokensRepository.create(token);
        emailHelper.constructResetTokenEmail(operatingUser, token);
    }

    @Override
    public Users getUserByToken(String token) {
        return tokensRepository.getUserByToken(token);
    }

    @Override
    public void changePassword(Users operatingUser, String password, String token) {
        if (operatingUser.getPassword().equals(password)) {
            throw new DuplicateEntityException("You have to enter a different password from the old one");
        }

        operatingUser.setPassword(password);
        usersRepository.update(operatingUser);

        //delete token after changing pass
        PasswordTokens tokenToDelete = tokensRepository.getTokenByContent(token);
        tokenToDelete.setUser(null);
        tokensRepository.update(tokenToDelete);
        tokensRepository.delete(tokenToDelete.getTokenId());
    }
}
