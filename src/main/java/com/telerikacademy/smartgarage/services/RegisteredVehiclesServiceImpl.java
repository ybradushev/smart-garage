package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.RegisteredVehicles;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.RegisteredVehicleRepository;
import com.telerikacademy.smartgarage.services.contracts.RegisteredVehiclesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegisteredVehiclesServiceImpl implements RegisteredVehiclesService {
    private final RegisteredVehicleRepository registeredVehicleRepository;

    @Autowired
    public RegisteredVehiclesServiceImpl(RegisteredVehicleRepository registeredVehicleRepository) {
        this.registeredVehicleRepository = registeredVehicleRepository;
    }

    @Override
    public RegisteredVehicles getRegisteredVehicleById(int id) {
        return registeredVehicleRepository.getByField("registeredVehicleId", id);
    }

    @Override
    public RegisteredVehicles getRegisteredVehicleByLicensePlate(String licensePlate) {
        return registeredVehicleRepository.getByField("licensePlate", licensePlate);
    }

    @Override
    public List<RegisteredVehicles> getVehiclesByOwner(int userId) {
        return registeredVehicleRepository.getVehiclesByOwner(userId);
    }

    @Override
    public List<RegisteredVehicles> getAllVehicles() {
        return registeredVehicleRepository.getAll();
    }

    @Override
    public void createRegisteredVehicle(RegisteredVehicles vehicle, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create new vehicles");
        }

        boolean vinExists = true;
        boolean licensePlateExists = true;

        try {
            registeredVehicleRepository.getByField("vin", vehicle.getVin());
        } catch (EntityNotFoundException e) {
            vinExists = false;
        }

        try {
            registeredVehicleRepository.getByField("licensePlate", vehicle.getLicensePlate());
        } catch (EntityNotFoundException e) {
            licensePlateExists = false;
        }

        if (licensePlateExists) {
            throw new DuplicateEntityException("vehicle", "license plate", vehicle.getLicensePlate());
        }

        if (vinExists) {
            throw new DuplicateEntityException("vehicle", "vin", vehicle.getVin());
        }

        registeredVehicleRepository.create(vehicle);
    }

    @Override
    public void updateRegisteredVehicle(RegisteredVehicles vehicle, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can update vehicles");
        }

        boolean vinExists = true;
        boolean licensePlateExists = true;

        try {
            registeredVehicleRepository.getByField("vin", vehicle.getVin());
        } catch (EntityNotFoundException e) {
            vinExists = false;
        }

        try {
            registeredVehicleRepository.getByField("licensePlate", vehicle.getLicensePlate());
        } catch (EntityNotFoundException e) {
            licensePlateExists = false;
        }

        if (licensePlateExists) {
            throw new DuplicateEntityException("vehicle", "license plate", vehicle.getLicensePlate());
        }

        if (vinExists) {
            throw new DuplicateEntityException("vehicle", "vin", vehicle.getVin());
        }

        registeredVehicleRepository.update(vehicle);
    }

    @Override
    public void deleteVehicle(int registeredVehicleId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete vehicles");
        }

        registeredVehicleRepository.delete(registeredVehicleId);
    }


    @Override
    public List<RegisteredVehicles> filter(Optional<Integer> year,
                                           Optional<Integer> brandId,
                                           Optional<Integer> modelId,
                                           Optional<String> sort,
                                           Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete users");
        }

        return registeredVehicleRepository.filter(year, brandId, modelId, sort);
    }

    @Override
    public List<RegisteredVehicles> search(Optional<String> value, Optional<String> type, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can search vehicles");
        }
        return registeredVehicleRepository.search(value, type);
    }


    private boolean isAdmin(Users operatingUser) {
        return operatingUser.getUserRole().getRoleName().equals("Employee");
    }


}
