package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.VisitStatuses;
import com.telerikacademy.smartgarage.repositories.contracts.VisitStatusesRepository;
import com.telerikacademy.smartgarage.services.contracts.VisitStatusesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VisitStatusesServiceImpl implements VisitStatusesService {
    private final VisitStatusesRepository visitStatusesRepository;

    @Autowired
    public VisitStatusesServiceImpl(VisitStatusesRepository visitStatusesRepository) {
        this.visitStatusesRepository = visitStatusesRepository;
    }

    @Override
    public VisitStatuses getStatusById(int id) {
        return visitStatusesRepository.getByField("statusId", id);
    }
}
