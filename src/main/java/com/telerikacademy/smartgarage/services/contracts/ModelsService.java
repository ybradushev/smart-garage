package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Models;
import com.telerikacademy.smartgarage.models.Users;

import java.util.List;

public interface ModelsService {
    Models getModelById(int id);

    List<Models> getAllModels();

    void createModel(Models model, Users operatingUser);

    void updateModel(Models model, Users operatingUser);

    void deleteModel(int modelId, Users operatingUser);

    List<Models> getModelsByBrandId(int brandId);
}
