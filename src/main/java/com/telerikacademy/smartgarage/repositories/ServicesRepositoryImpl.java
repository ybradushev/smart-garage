package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ServicesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ServicesRepositoryImpl extends AbstractCRUDRepository<Services> implements ServicesRepository {

    public ServicesRepositoryImpl(SessionFactory sessionFactory) {
        super(Services.class, sessionFactory);
    }

    @Override
    public List<Services> getAllServices() {
        try (Session session = sessionFactory.openSession()) {
            Query<Services> query = session.createQuery("from Services where isDeleted = false", Services.class);
            return query.list();
        }
    }

    @Override
    public List<Services> filter(Optional<String> serviceName, Optional<Double> minPrice, Optional<Double> maxPrice,
                                 Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Services ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            serviceName.ifPresent(value -> {
                if (!serviceName.get().equals("")) {
                    filter.add(" serviceName like :serviceName");
                    queryParams.put("serviceName", "%" + value + "%");
                }
            });

            minPrice.ifPresent(value -> {
                filter.add(" price >= :minPrice ");
                queryParams.put("minPrice", value);
            });

            maxPrice.ifPresent(value -> {
                filter.add(" price <= :maxPrice ");
                queryParams.put("maxPrice", value);
            });

            if (!filter.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", filter));
                System.out.println(queryString);
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            System.out.println(queryString);

            Query<Services> query = session.createQuery(queryString.toString(), Services.class);
            query.setProperties(queryParams);

            return query.list();
        }
    }

    @Override
    public List<Services> search(Optional<String> keyword) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from Services ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            keyword.ifPresent(value -> {
                filter.add(" serviceName like :serviceName");
                queryParams.put("serviceName", "%" + value + "%");
            });

            if (!filter.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", filter));
                System.out.println(queryString);
            }

            Query<Services> query = session.createQuery(queryString.toString(), Services.class);
            query.setProperties(queryParams);

            return query.list();
        }
    }

    @Override
    public void deleteService(int serviceId) {
        Services serviceToDelete = super.getById(serviceId);
        try (Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            serviceToDelete.setDeleted(true);
            session.update(serviceToDelete);
            session.getTransaction().commit();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        switch (params[0]) {
            case "name":
                queryString.append(" serviceName ");
                break;
            case "price":
                queryString.append(" price ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        return queryString.toString();
    }
}
