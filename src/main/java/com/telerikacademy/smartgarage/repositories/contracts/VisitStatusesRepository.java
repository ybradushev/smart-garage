package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.VisitStatuses;

public interface VisitStatusesRepository extends BaseCRUDRepository<VisitStatuses> {
}
