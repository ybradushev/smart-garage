package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.VisitStatuses;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitStatusesRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class VisitStatusesRepositoryImpl extends AbstractCRUDRepository<VisitStatuses> implements VisitStatusesRepository {
    public VisitStatusesRepositoryImpl(SessionFactory sessionFactory) {
        super(VisitStatuses.class, sessionFactory);
    }
}
