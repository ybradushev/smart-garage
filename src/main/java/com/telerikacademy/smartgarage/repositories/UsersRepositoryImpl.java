package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UsersRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@Repository
public class UsersRepositoryImpl extends AbstractCRUDRepository<Users> implements UsersRepository {

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        super(Users.class, sessionFactory);
    }

    @Override
    public List<Users> search(Optional<String> keyword, Optional<String> type) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder();
            var queryParams = new HashMap<String, Object>();
            queryString.append(" from Users where userRole.userRoleId = 1 and userId <> 16 ");

            keyword.ifPresent(fieldValue -> {
                if (type.get().equals("name")) {
                    queryString.append("and concat(firstName, ' ', lastName) like :value");
                    queryParams.put("value", "%" + fieldValue + "%");
                } else if (type.get().equals("email")) {
                    queryString.append("and email = :value");
                    queryParams.put("value", fieldValue);
                } else if (type.get().equals("phoneNumber")) {
                    queryString.append("and phoneNumber = :value");
                    queryParams.put("value", fieldValue);
                }
            });

            Query<Users> query = session.createQuery(queryString.toString(), Users.class);
            query.setProperties(queryParams);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("No users matched to criteria");
            }
            return query.list();
        }
    }

    @Override
    public List<Users> getAllCustomers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery("from Users where userRole.roleName = :roleName and userId <> 16", Users.class);
            query.setParameter("roleName", "User");

            return query.list();
        }
    }
}
