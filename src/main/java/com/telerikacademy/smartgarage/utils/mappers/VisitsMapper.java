package com.telerikacademy.smartgarage.utils.mappers;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.*;
import com.telerikacademy.smartgarage.models.dto.*;
import com.telerikacademy.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class VisitsMapper {
    private final ServicesService servicesService;
    private final VehiclesService vehiclesService;
    private final VisitStatusesService statusesService;
    private final RegisteredVehiclesService registeredVehiclesService;
    private final UsersService usersService;
    private final UserMapper userMapper;
    private final VehicleMapper vehicleMapper;

    @Autowired
    public VisitsMapper(ServicesService servicesService, VehiclesService vehiclesService1,
                        VisitStatusesService statusesService, RegisteredVehiclesService vehiclesService,
                        UsersService usersService, UserMapper userMapper, VehicleMapper vehicleMapper) {
        this.servicesService = servicesService;
        this.vehiclesService = vehiclesService1;
        this.statusesService = statusesService;
        this.registeredVehiclesService = vehiclesService;
        this.userMapper = userMapper;
        this.usersService = usersService;
        this.vehicleMapper = vehicleMapper;
    }

    public Visits createDtoToObject(CreateVisitDTO visitDTO, Users operatingUser) {
        double totalPrice = 0;
        Visits visit = new Visits();

        Users owner;
        try {
            owner = usersService.getByEmail(visitDTO.getEmail());
        } catch (EntityNotFoundException e) {
            owner = userMapper.createDTOToObject(new CreateUserDTO(visitDTO.getFirstName(), visitDTO.getLastName(),
                    visitDTO.getEmail(), visitDTO.getPhone()));
            usersService.createUser(owner, operatingUser);
        }

        Vehicles vehicle;
        try {
            vehicle = vehiclesService.getVehicleByModelAndYear(visitDTO.getModelId(), visitDTO.getYear());
        } catch (EntityNotFoundException e) {
            vehicle = vehicleMapper.createVehicleDTOtoObject(new VehicleDTO(visitDTO.getModelId(), visitDTO.getYear()));
            vehiclesService.createVehicle(vehicle, operatingUser);
        }

        RegisteredVehicles registeredVehicle;
        try {
            registeredVehicle = registeredVehiclesService.getRegisteredVehicleByLicensePlate(visitDTO.getLicensePlate());
        } catch (EntityNotFoundException e) {
            registeredVehicle = vehicleMapper.createRegVehicleDTOtoObject(new CreateRegVehicleDTO(visitDTO.getVin(),
                    visitDTO.getLicensePlate(), owner.getUserId(), vehicle.getVehicleId()));
            registeredVehiclesService.createRegisteredVehicle(registeredVehicle, operatingUser);
        }

        visit.setRegisteredVehicle(registeredVehicle);
        visit.setStartDate(LocalDate.now());
        visit.setStatus(statusesService.getStatusById(1));

        for (Integer servicesId : visitDTO.getServiceIds()) {
            Services serviceToAdd = servicesService.getServiceById(servicesId);
            totalPrice += serviceToAdd.getPrice();
            visit.addServiceToSet(serviceToAdd);
        }

        visit.setTotalPrice(totalPrice);
        return visit;

    }

    public Visits createDtoToObject(VisitDTO visitDTO) {
        double totalPrice = 0;
        Visits visit = new Visits();

        visit.setStartDate(LocalDate.now());
        visit.setStatus(statusesService.getStatusById(1));

        for (Integer servicesId : visitDTO.getServicesIds()) {
            Services serviceToAdd = servicesService.getServiceById(servicesId);
            totalPrice += serviceToAdd.getPrice();
            visit.addServiceToSet(serviceToAdd);
        }

        visit.setTotalPrice(totalPrice);
        return visit;
    }
}
