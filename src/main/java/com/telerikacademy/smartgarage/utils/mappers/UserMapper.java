package com.telerikacademy.smartgarage.utils.mappers;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.UserRole;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.dto.CreateUserDTO;
import com.telerikacademy.smartgarage.models.dto.UpdateUserDTO;
import com.telerikacademy.smartgarage.services.UserRolesServiceImpl;
import com.telerikacademy.smartgarage.services.contracts.UserRolesService;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class UserMapper {
    private final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*&+])(?=\\S+$).{6,}$";
    private final UserRolesService userRolesService;
    private final UsersService userService;

    @Autowired
    public UserMapper(UserRolesServiceImpl userRolesService, UsersService userService) {
        this.userRolesService = userRolesService;
        this.userService = userService;
    }


    public Users createDTOToObject(CreateUserDTO userDTO) {
        Users user = new Users();

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setUsername(generateUsername(userDTO.getEmail()));
        user.setPassword(generatePassword());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        UserRole userRole = userRolesService.getRoleById(1);
        user.setUserRole(userRole);

        return user;
    }

    public Users updateDTOToObject(UpdateUserDTO userDTO, int userId) {
        Users user = userService.getUserById(userId);

        user.setEmail(userDTO.getEmail());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        return user;
    }

    private String generateUsername(String email) {
        int counterForUsername = 1;
        boolean usernameExists = true;
        int indexOfAt = email.indexOf('@');
        String username = email.substring(0, indexOfAt);

        do {
            try {
                userService.getUserByUsername(username);
                username += counterForUsername;
                counterForUsername++;
            } catch (EntityNotFoundException e) {
                usernameExists = false;
            }
        } while (usernameExists);

        System.out.println(username);
        return username;
    }

    private String generatePassword() {
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String alphaNumeric = upperAlphabet + lowerAlphabet + numbers;

        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < 8; i++) {
            int index = random.nextInt(alphaNumeric.length());
            char randomChar = alphaNumeric.charAt(index);
            sb.append(randomChar);
        }

        return sb.toString();
    }
}

