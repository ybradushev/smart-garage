package com.telerikacademy.smartgarage.utils.helpers;

import com.telerikacademy.smartgarage.exceptions.users.PasswordTokenExpiredException;
import com.telerikacademy.smartgarage.models.PasswordTokens;
import com.telerikacademy.smartgarage.repositories.contracts.PasswordTokensRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ValidatePasswordTokenHelper {
    private final PasswordTokensRepository tokensRepository;

    public ValidatePasswordTokenHelper(PasswordTokensRepository tokensRepository) {
        this.tokensRepository = tokensRepository;
    }

    public void validatePasswordResetToken(String token) {
        PasswordTokens passToken = tokensRepository.getTokenByContent(token);

        isTokenExpired(passToken);
    }

//    private boolean isTokenFound(PasswordTokens passToken) {
//        return passToken != null;
//    }

    private void isTokenExpired(PasswordTokens passToken) {
        if (passToken.getExpirationDate().isBefore(LocalDateTime.now())) {
            throw new PasswordTokenExpiredException();
        }
    }
}
