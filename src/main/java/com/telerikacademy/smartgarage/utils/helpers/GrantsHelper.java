package com.telerikacademy.smartgarage.utils.helpers;

import com.telerikacademy.smartgarage.models.Users;

public class GrantsHelper {

    public static boolean isAdmin(Users operatingUser) {
        return operatingUser.getUserRole().getRoleName().equals("Employee");
    }
}
