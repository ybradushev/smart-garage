package com.telerikacademy.smartgarage.utils.helpers;

import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.services.contracts.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CurrencyHelper {
    private final ServicesService servicesService;

    @Autowired
    public CurrencyHelper(ServicesService servicesService) {
        this.servicesService = servicesService;
    }

    public Services convertServiceCurrency(Services service, String currency) {
        String url = "https://free.currconv.com/api/v7/convert?q=BGN_" +
                currency
                + "&compact=ultra&apiKey=03c72c926d0419220dc8";
        RestTemplate rest = new RestTemplate();
        String response = rest.getForObject(url, String.class);
        assert response != null;

        response = response.substring(11, response.length() - 1);

        double price = Double.parseDouble(response) * service.getPrice();
        service.setPrice(price);

        return service;
    }
}
