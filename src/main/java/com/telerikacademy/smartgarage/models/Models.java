package com.telerikacademy.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "vehicles_models")
public class Models {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "model_id")
    private int modelId;

    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brands brand;

    @Column(name = "model_name")
    private String modelName;

    public Models() {
    }

    public Models(int modelId, Brands brand, String modelName) {
        this.modelId = modelId;
        this.brand = brand;
        this.modelName = modelName;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public Brands getBrand() {
        return brand;
    }

    public void setBrand(Brands brand) {
        this.brand = brand;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }
}
