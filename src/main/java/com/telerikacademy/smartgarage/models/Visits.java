package com.telerikacademy.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "visits")
public class Visits {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int visitId;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "total_price")
    private double totalPrice;

    @ManyToOne
    @JoinColumn(name = "registered_vehicle_id")
    private RegisteredVehicles registeredVehicle;

    @ManyToOne
    @JoinColumn(name = "visit_status")
    private VisitStatuses status;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "services_history",
            joinColumns = @JoinColumn(name = "visit_id"),
            inverseJoinColumns = @JoinColumn(name = "service_id")
    )
    private Set<Services> servicesList;

    public Visits() {
        servicesList = new HashSet<>();
    }

    public Visits(int visitId, LocalDate startDate, LocalDate endDate, double totalPrice,
                  RegisteredVehicles registeredVehicle, VisitStatuses status, Set<Services> servicesList) {
        this.visitId = visitId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalPrice = totalPrice;
        this.registeredVehicle = registeredVehicle;
        this.status = status;
        this.servicesList = servicesList;
    }

    public int getVisitId() {
        return visitId;
    }

    public void setVisitId(int visitId) {
        this.visitId = visitId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public RegisteredVehicles getRegisteredVehicle() {
        return registeredVehicle;
    }

    public void setRegisteredVehicle(RegisteredVehicles registeredVehicle) {
        this.registeredVehicle = registeredVehicle;
    }

    public VisitStatuses getStatus() {
        return status;
    }

    public void setStatus(VisitStatuses status) {
        this.status = status;
    }

    public Set<Services> getServicesList() {
        return servicesList;
    }

    public void setServicesList(Set<Services> servicesList) {
        this.servicesList = servicesList;
    }

    public void addServiceToSet(Services service) {
        this.servicesList.add(service);
    }

    public void removeServiceFromSet(Services service) {
        this.servicesList.remove(service);
    }
}
