package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterDTO extends LoginDTO {

    @NotEmpty(message = "First name cannot be empty")
    @Size(min = 4, max = 32, message = "First name should be between 4 and 32 characters")
    private String firstName;

    @NotEmpty(message = "Last name cannot be empty")
    @Size(min = 4, max = 32, message = "Last name should be between 4 and 32 characters")
    private String lastName;

    @NotEmpty(message = "Email cannot be empty")
    @Email(message = "Email is not valid", regexp = "^(.+)@(.+)$")
    private String email;

    @NotEmpty(message = "Password confirmation can't be empty")
    private String confirmPassword;


    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
