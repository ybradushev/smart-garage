package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class EmailDTO {

    @NotNull(message = "Email cannot be empty")
    @Email(message = "Email is not valid", regexp = "^(.+)@(.+)$")
    private String email;

    public EmailDTO() {
    }

    public EmailDTO(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
