package com.telerikacademy.smartgarage.models;

public class MockService {
    private int serviceId;

    public MockService() {
    }

    public MockService(int serviceId) {
        this.serviceId = serviceId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }
}
