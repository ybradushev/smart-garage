package com.telerikacademy.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "registered_vehicles")
public class RegisteredVehicles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "registered_vehicle_id")
    private int registeredVehicleId;

    @Column(name = "license_plate")
    private String licensePlate;

    @Column(name = "vin")
    private String vin;

    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicles vehicle;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Users owner;


    public RegisteredVehicles() {
    }

    public RegisteredVehicles(int registeredVehicleId, String licensePlate, String vin, Vehicles vehicle, Users owner) {
        this.registeredVehicleId = registeredVehicleId;
        this.licensePlate = licensePlate;
        this.vin = vin;
        this.vehicle = vehicle;
        this.owner = owner;
    }

    public int getRegisteredVehicleId() {
        return registeredVehicleId;
    }

    public void setRegisteredVehicleId(int registeredVehicleId) {
        this.registeredVehicleId = registeredVehicleId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Vehicles getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicles vehicle) {
        this.vehicle = vehicle;
    }

    public Users getOwner() {
        return owner;
    }

    public void setOwner(Users owner) {
        this.owner = owner;
    }
}
